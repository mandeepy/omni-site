import React from "react"
import Swal from 'sweetalert2'

import LazyImage from "../components/lazyLoad";
import Layout from "../components/layout"
import SEO from "../components/seo"
import '../scss/style.scss'

import { TEAM, PROJECT, PRODUCT, CURRENCY_FEATURE, FEATURE } from '../locales'

const youtubePopup = () => {
  Swal.fire({
    showCloseButton: true,
    showConfirmButton: false,
    width: 600,
    html: '<iframe width="100%" height="300" src="https://www.youtube.com/embed/VIrZMIj4glc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
  })
}

const IndexPage = () => (
  <Layout page="home">
    <SEO title="Home" />
      <section className="leading-content">
        <div className="content-wrapper">
          <div className="content" data-aos="fade-up">
            <h1>Fully-Decentralized Asset Platform</h1>
            <p>Omni is a platform for creating and trading custom digital assets and currencies.</p>
            <div className="action-item video-link">
              <div role="button" tabIndex={0} onClick={youtubePopup} onKeyDown={()=> {}}>
                <span>Watch Video</span>
                <LazyImage alt="https://youtu.be/VIrZMIj4glc" src="/images/video-preview.png"/>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="about" className="about-content full-screen">
        <div className="content-wrapper">
          <div className="trustees" data-aos="fade-up">
            <p className="trust-label">We are trusted by</p>
            <div className="logo-images">
              <LazyImage alt="Bitcoin Magazine" src="/images/bitcoin-magazine.svg"/>
              <LazyImage alt="Forbes" src="/images/forbes.png"/>
              <LazyImage alt="Coindesk" src="/images/coindesk.svg"/>
              <LazyImage alt="Yahoo" src="/images/yahoo.png"/>
            </div>
            <h3>Built on top of the Bitcoin blockchain</h3>
            <p>
              Omni is a platform for creating and trading custom digital assets and currencies. 
It is a software layer built on top of the most popular, most audited, most secure blockchain -- Bitcoin. Omni transactions are Bitcoin transactions that enable next-generation features on the Bitcoin Blockchain.
            </p>
          </div>
        </div>
      </section>
      <section className="features">
        <div className="content-wrapper"> 
          {FEATURE.map(({title, description, additionalClass, text, image, link})=>(
            <div key={`wrapper-${additionalClass}`} className={`feature ${additionalClass}`}>
              <div className="feature-image">
                <LazyImage
                  alt={title}
                  src={image}
                  data-aos="zoom-in-down"
                  data-aos-anchor={`.${additionalClass}`}
                  />
              </div>
              <div className="feature-text" data-aos="fade-up">
                <h3>{title}</h3>
                <p>{description}</p>
                <a
                  href={link}
                  rel="noopener noreferrer"
                  target="_blank"
                  >
                  {text}
                  <span className="arrow">></span>
                </a>
              </div>
            </div>
          ))}
        </div>
      </section>
      <section className="features-small-section">
        <div className="content-wrapper">
          <h3>Custom Currency Can Be Made Simple.</h3>
          <div className="right"  data-aos="fade-up">
            <ul>
              {CURRENCY_FEATURE.map(({image,title})=>(
                <li key={`list-${image}`}>
                  <LazyImage alt={title} src={image}/>
                  <span>{title}</span>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </section>
      <section id="products" className="products-section" >
         <div className="content-wrapper">
         <div className="heading">
            <h3> Our Products </h3>
            <p>Start with a web application or a software download.</p>
          </div>
          <div className="product-listing" data-aos="fade-up" data-aos-anchor='.products-section'>
            {PRODUCT.map(({additionalClass,image,title,list,link,text})=>(
              <div key={`wrapper-${additionalClass}`} className={`listing ${additionalClass}`}>
                 <div className="listing-name">
                     <LazyImage alt={title} src={image}/>
                     <h4>{title}</h4>
                 </div>
                 <div className="listing-features">
                   <ul>{list.map((node, i)=><li key={`feature-list${i}`}>{node}</li>)}</ul>
                 </div>
                 <a href={link} rel="noopener noreferrer" target="_blank">
                 {text} <span className="arrow">></span></a>
              </div>
            ))}
          </div>
         </div>
      </section>
      <section id="projects" className="projects-section">
          <div className="content-wrapper">
              <div className="left">
                <div className="github-branding">
                  <h3> Github Projects </h3>
                  <LazyImage alt="Github logo" src="/images/github.svg"/>
                </div>
              </div>
              <div className="right" data-aos="fade-up">
                {PROJECT.map(({title, description, url, alt, image})=>(
                  <a key={`link-${title}`} href={url} rel="noopener noreferrer" target="_blank">
                    <div className="project">
                      <LazyImage alt={alt} src={image}/>
                      <p className="project-title">{title}</p>
                      <p className="project-description">{description}</p>
                    </div>
                  </a>
                ))}
              </div>
          </div>
      </section>
      <section id="testimonials" className="testimonial-section">
        <div className="image">
        </div>
        <div className="content-wrapper">
          <div className="quote" data-aos="fade-up">
            <img alt="" className="start-quote" src="/images/start-quotes.svg"/>
            <p>With Omni and Tether, this was probably one of the nicest integrations I’ve ever done with a blockchain asset. Having the original bitcoin RPC on top of the additional commands was exactly how it should be.</p>
            <img alt="" className="end-quote" src="/images/end-quotes.svg"/>
            <div className="testimonial-writer">
              <p className="writer">Matt D</p>
              <p className="company">shapeshift.io</p>
            </div>
          </div>
        </div>
      </section>
      <section id="team" className="team-section">
        <div className="content-wrapper">
          <div className="team-heading">
          <h3> Our Team </h3>
          <p> Meet the people who are working on improving the Omni Layer. </p>
          </div>
          <div className="our-team">
            {TEAM.map(({name, image, title})=>(
              <div key={`tm-${name}`} className='team-member'>
                <div className="photo">
                  <LazyImage alt={name} src={image}/>
                </div>
                <p className="name">{name}</p>
                <p className="title">{title}</p>
              </div>
            ))}  
          </div>
        </div>
      </section>
      <footer>
        <div className="partners-wrapper">
        <div className="partners">
          <p>Our Partners</p>
          <a href="https://www.ambisafe.co/" target="_blank" rel="noopener noreferrer"><LazyImage alt="ambisafe.co" src="/images/ambisafe.png"/></a>
          <a href="https://holytransaction.com/" target="_blank" rel="noopener noreferrer"><LazyImage alt="holytransation.com" src="/images/holytransaction.png"/></a>
        </div>
        </div>
        <div className="social-media">
          <div className="sites">
            <p><a href="https://www.facebook.com/groups/1501816626750032/" target="_blank" rel="noopener noreferrer">Facebook</a></p>
            <p><a href="https://twitter.com/Omni_layer" target="_blank" rel="noopener noreferrer">Twitter</a></p>
            <p><a href="https://t.me/OmniLayer" target="_blank" rel="noopener noreferrer">Telegram</a></p>
            <p><a href="https://www.reddit.com/r/omni/" target="_blank" rel="noopener noreferrer">Reddit</a></p>
          </div>
          <p>© {new Date().getFullYear()} Omni Team.  All Rights Reserved.</p>
        </div>
      </footer>
  </Layout>
)

export default IndexPage
