import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { BlogPostCard, FeaturedPostCard } from "../components/PostList"
import EmailListForm from "../components/emailBox"
import chunk from "lodash/chunk"

class Blog extends React.Component {
  constructor(props) {
    super(props);
    let postsToShow = 4
    // to not repeat featured post.
    let totalPosts = props.data.normalPostsQuery.edges.length - 1;
    this.state = {
      postsToShow,
      showingMore: postsToShow < totalPosts,
      allPosts: totalPosts,
    }
  }

  morePosts=()=>{
    this.setState({
      postsToShow: this.state.postsToShow + 4,
      showingMore: this.state.postsToShow + 4 < this.state.allPosts,
    })
  }

  organizePosts=(normalPosts={edges:[]}, featuredPost=[])=>{
    const result1=[featuredPost.frontmatter].map((chunk, i) => {
      if(chunk){
      const {path,title,date,cover,featured,author_name}=chunk;
      return (
        <FeaturedPostCard
          key={i}
          path={path}
          title={title}
          date={date}
          cover={cover.childImageSharp.fluid}
          featured={featured}
          author_name={author_name}
        />
      )}
      return null;
    });
    const filteredPosts=normalPosts.edges.filter((post=>{
      const n= post;
      return n.node.id !== featuredPost.id
    }));
    const result2 = chunk(filteredPosts.slice(0, this.state.postsToShow), 1)
    .map((chunk, i) => {
      const post = chunk[0];
      return (
        <BlogPostCard
          key={i+1}
          path={post.node.frontmatter.path}
          title={post.node.frontmatter.title}
          date={post.node.frontmatter.date}
          cover={post.node.frontmatter.cover.childImageSharp.fluid}
          featured={post.node.frontmatter.featured}
          author_name={post.node.frontmatter.author_name}
        />
      )
    })
    const data=[...result1,...result2];
    data.splice(3, 0, <EmailListForm />);
    return data;
  }

  render() {
    const { data } = this.props;
    const availableFeaturedPost = data.featuredPostQuery.edges || {};
    const {normalPostsQuery: normalPosts} = data;
    let featuredPost;
    try{
      featuredPost = availableFeaturedPost.length
      ? availableFeaturedPost[0].node
      : normalPosts.edges[0].node;
    } catch {
      featuredPost=[];
    }
    const organizedPosts=this.organizePosts(normalPosts, featuredPost);
    return (
      <Layout page="blog">
        <SEO title="Blog Page" />
        <div className="blog" data-aos="fade-up">
          <h1 className="blog-title">Blog</h1>
          <div className="all-posts">
            <div className="featured">
              {organizedPosts[0]
                ? organizedPosts.shift()
                : <div className="no-posts"><span>There are no posts for now!</span></div>}
            </div>
            <div className="posts">
              {organizedPosts}
            </div>
          </div>
          {this.state.showingMore && (
            <button id="more-posts-btn" onClick={this.morePosts}>
                More Articles <span className="arrow">></span>
            </button>)}
        </div>
      </Layout>
    )
  }
}

export const pageQuery = graphql`
query indexQuery {
  normalPostsQuery: allMarkdownRemark(
    filter: {frontmatter: {published: {eq: true}}},
    sort: {order: DESC, fields: [frontmatter___date]}
  ){
    edges {
      node {
        id
        frontmatter {
          title
          path
          date
          featured
          author_name
          cover {
            childImageSharp {
              fluid(
                quality: 100,
                pngQuality: 100,
                jpegQuality: 100,
                webpQuality: 100
                ) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
  featuredPostQuery: allMarkdownRemark(
    sort: {order: DESC, fields: [frontmatter___date]},
    filter: {
      frontmatter: {
        published: {eq: true},
        featured: {eq: true}
      }
    },
    limit: 1
    ) {
    edges {
      node {
        html
        id
        frontmatter {
          path
          title
          date
          featured
          author_name
          cover {
            childImageSharp {
              fluid(
                quality: 100,
                pngQuality: 100,
                jpegQuality: 100,
                webpQuality: 100
                ) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
}
`

export default Blog