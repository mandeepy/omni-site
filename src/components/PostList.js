import React from 'react';
import { Link } from 'gatsby';
import PropTypes from 'prop-types';

const PostCard = ({ path, title, cover, author_name, isFeatured }) => {
  const featured = isFeatured? 'featured-' : '';
  return (
    <>
    <Link to={path}>
      <div className={`${featured}post-card`}>
        
        <div className="image" style={{backgroundImage: 'url(' + cover.src + ')'}}>
        </div>
        
        <div className={`${featured}caption-box`}>
          <p className="author">By {author_name}</p>
          <p className="title">{title}</p>
        </div>
      </div>
    </Link>
    </>
  );
}

export const FeaturedPostCard=(props)=>(<PostCard {...props} isFeatured />)

export const BlogPostCard=(props)=>(<PostCard {...props} />)

export default BlogPostCard;

PostCard.propTypes = {
  path: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  cover: PropTypes.instanceOf(Object),
  author_name: PropTypes.string.isRequired,
  isFeatured: PropTypes.bool,
};

PostCard.defaultProps = {
  cover: {},
  isFeatured: false,
};
