import React from "react";
import LazyLoad from "vanilla-lazyload";

const lazyloadConfig = {
  elements_selector: ".lazy"
};

export class LazyImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lazyLoadInstance: null,
    }
  }

  // Update lazyLoad after first rendering of every image
  componentDidMount() {
    const { lazyLoadInstance } = this.state;
    lazyLoadInstance ? lazyLoadInstance.update() :
      this.setState({ lazyLoadInstance : new LazyLoad(lazyloadConfig) }, 
        () => { this.state.lazyLoadInstance.update(); });
  }

  // Update lazyLoad after rerendering of every image
  componentDidUpdate() {
    const { lazyLoadInstance } = this.state;
    lazyLoadInstance.update();
  }

  // Just render the image with data-src
  render() {
    const { alt, src, srcset, sizes, width, height } = this.props;
    return (
      <img
        alt={alt}
        className="lazy"
        data-src={src}
        data-srcset={srcset}
        data-sizes={sizes}
        width={width}
        height={height}
        data-aos={this.props['data-aos']}
        data-aos-anchor={this.props['data-aos-anchor']}
      />
    );
  }
}

export default LazyImage;
