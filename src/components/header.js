import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

import { HEADER_LINKS } from '../locales'

const smoothScroll = (element) => {
  document.querySelector('.menu-icon').click();
  const menuItem = document.querySelector(`a[href="/${element}"]`);
  const prevMenu = document.querySelector('.active-menu-item');
  if(prevMenu) prevMenu.classList.remove('active-menu-item');

  menuItem.classList.add('active-menu-item');
  // adds #hash-links when navbar links are clicked on homepage.
  window.history.pushState({},"", element);
  window.scrollTo({
    top: document.querySelector(element).offsetTop,
    left: 0,
    behavior: 'smooth'
  });
}

const toggleMenu = (e) => {
  const icon = e.currentTarget;
  icon.classList.toggle('open');
  document.querySelector('.menu').classList.toggle('open');
  document.querySelector('body').classList.toggle('open-menu');
}


const Header = ({isHome}) => (
  <div className="top-bar" data-aos="fade-down">
    <div className="nav">
      <div className="branding">
        <Link to="/">
          <img src="/images/logo.svg" alt="logo" />
          <img src="/images/omni-text.svg" className="text-logo" alt="logo-text" />
        </Link>
      </div>

      <div tabIndex={0} role="button" className="menu-icon" onClick={toggleMenu} onKeyDown={toggleMenu}>
        <span className="top"></span>
        <span className="middle"></span>
        <span className="bottom"></span>
      </div>
      <div className="menu">
        <ul>
          {HEADER_LINKS.map(({title,id})=>(
            <li key={title}>
              <Link
                to={`/${id}`}
                onClick={ e => {
                  if (isHome) {
                    e.preventDefault();
                    smoothScroll(id);
                  }
              }}>
                {title}
              </Link>
            </li>
          ))}
          <li key="blog">
            <Link to="/blog">
              Blog
            </Link>
          </li>
        </ul>
      </div>
    </div>
  </div>
)



Header.propTypes = {
  isHome: PropTypes.bool,
}

Header.defaultProps = {
  isHome: true,
}

export default Header
