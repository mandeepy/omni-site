import React from 'react';
import Swal from 'sweetalert2'
import addToMailchimp from 'gatsby-plugin-mailchimp';

class EmailListForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    }

  }
  handleSubmit = (e) => {
    e.preventDefault();

    addToMailchimp(this.state.email)
      .then((data) => {
        // console.log(data);
        // alert(data.result);
        if (data.result === "success") {
          this.setState({email: ''});
          return Swal.fire({
            icon: 'success',
            title: "Great!",
            text: "You've been subscribed successfully",
          })
        }
        let message = data.msg || 'Something went wrong!'
        return Swal.fire({
          icon: 'error',
          title: 'Oops...',
          html: message,
        })
      })
      .catch((error) => {
        // Errors in here are client side
        // Mailchimp always returns a 200
        console.log("error");
      });
  };

  handleEmailChange = (event) => {
    this.setState({email: event.currentTarget.value});
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit} id="emailbox-form">
        <h2 id="email-description">Get the notification when new article released</h2>
        <div id="email-box">
          <input
            placeholder="Enter email"
            name="email"
            type="text"
            onChange={this.handleEmailChange}
            id="email-input"
            required
            value={this.state.email}
          />
          <button type="submit" id="email-submit">Submit <span className="arrow">></span></button>
        </div>
      </form>
    );
  }
};

export default EmailListForm;