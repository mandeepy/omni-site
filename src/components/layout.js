import React from "react"
import PropTypes from "prop-types"

import Header from "./header"
import "../scss/style.scss"
import AOS from 'aos';
import 'aos/dist/aos.css';

class Layout extends React.Component {
  constructor(props) {
    super(props);
    if (typeof window !== `undefined`) {
      AOS.init({
        duration: 600,
        easing: 'ease-in-sine',
        offset: -100,
        delay: 200,
      });
    }
  }

  static propTypes = {
    children: PropTypes.node.isRequired,
  }

  componentDidUpdate() {
    if (typeof window !== `undefined`) {
      AOS.refresh();
    }
  }

  render() {
    const { page, children } = this.props;
    return (
      <div className="page-wrapper">
        <Header isHome={page === 'home'} />
        <main>{children}</main>
      </div>
    )
  }
};

export default Layout
