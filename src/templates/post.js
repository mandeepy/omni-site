import React from 'react';
import { graphql } from 'gatsby'
import Layout from '../components/layout';
import SEO from "../components/seo";

function dateFormat(inputDate) {
    // '2020-01-21T18:30:00Z'
    let jsDate = new Date(inputDate);
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    return jsDate.toLocaleDateString('en', options);
}

export default function Template({data}) {
    const {markdownRemark: post} = data;
    return (
        <Layout>
            <SEO title={post.frontmatter.title} />
            <div className="post">
                <div className="post-header">
                    <h1 className="post-title">{post.frontmatter.title}</h1>
                    <p className="date">Posted on {dateFormat(post.frontmatter.date)}</p>
                    <p className="sub-title">{post.frontmatter.sub_title}</p>
                </div>
                <div className="post-content" dangerouslySetInnerHTML={{__html: post.html}} />
                <div className="post-author">
                    <p>Written By</p>
                    <img src={post.frontmatter.author_image.childImageSharp.fluid.src} alt="author img" />
                    <p className="author-name">{post.frontmatter.author_name}</p>
                </div>
            </div>
        </Layout>
    )
}

export const postQuery = graphql`
    query BlogPostByPath($path: String!) {
        markdownRemark(frontmatter: { path: { eq: $path } }) {
            html
            frontmatter {
                path
                title
                sub_title
                date
                author_name
                author_image {
                    childImageSharp {
                        fluid(
                            quality: 100,
                            pngQuality: 100,
                            jpegQuality: 100,
                            webpQuality: 100
                            ) {
                            ...GatsbyImageSharpFluid
                        }
                        }
                }
            }
        }
    }
`