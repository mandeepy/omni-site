export const FEATURE = [
  {
    title:'Easily create custom currencies',
    description:'With Omni it\'s simple to create tokens to represent custom currencies or assets and to transact these via the Bitcoin blockchain. The power and simplicity offered by Omni has helped to make it the leading Bitcoin based token protocol.',
    additionalClass:'feature-1',
    link:'https://www.omniexplorer.info/properties/production',
    text:'Learn More',
    image:'images/rectangle_looper.svg',
  },
  {
    title:'Blockchain based crowdfunding',
    description:'Decentralized crowd funding is easy with Omni. Crowd sale participants can send bitcoins or tokens directly to an issuer address and the Omni Layer automatically delivers the crowdfunded tokens to the sender in return - all without needing to trust a third party.',
    additionalClass:'feature-2',
    link:'https://www.omniexplorer.info/crowdsales/production',
    text:'Learn More',
    image:'images/oval_looper.svg',
  },
  {
    title:'Trade peer-to-peer',
    description:'Participants can use the distributed exchanges provided by the Omni Layer to exchange tokens for other tokens or bitcoins directly on the blockchain without the need for a third party exchange.',
    additionalClass:'feature-3',
    link:'https://blog.omni.foundation/2016/03/16/omnidex-getting-started/',
    text:'Get Started',
    image:'images/Combined_Shape_looper.svg',
  }
]