export const TEAM=[
  {
    name:'Adam',
    image:'images/team/adam.png',
    title:'Founder'
  },
  {
    name:'Ben',
    image:'images/team/ben.png',
    title:'Co-Founder'
  },
  {
    name:'Craig',
    image:'images/team/craig.png',
    title:'Co-Founder'
  },
  {
    name:'Dexx7',
    image:'images/team/dex.png',
    // title:'Project Manager'
  },
  {
    name:'Germán',
    image:'images/team/german.png',
    // title:'Founder'
  },
  {
    name:'Judith',
    image:'images/team/judith.png',
    title:'Head of Recruiting'
  },
  {
    name:'Marv',
    image:'images/team/marv.png',
    title:'Project Manager'
  },
  {
    name:'Peter',
    image:'images/team/peter.png',
    title:'Project Lead'
  },
  {
    name:'Sean',
    image:'images/team/sean.png',
    // title:'Founder'
  },
  {
    name:'Yann',
    image:'images/team/yann.png',
    // title:'Founder'
  },
]