export const PRODUCT=[
    {
      additionalClass: "listing-1",
      image: "images/logo.svg",
      title: "Omni Wallet",
      list: [
        "Free, hosted web wallet",
        "You control your private keys",
        "Send and receive Bitcoin or Omni assets",
        "Create assets, launch crowdsales, and trade on the distributed exchange"
      ],
      link: "https://www.omniwallet.org/",
      text: "Get Started"
    },
    {
      additionalClass: "listing-2",
      image: "images/logo.svg",
      title: "Omni Explorer",
      list: [
        "Omni blockchain explorer",
        "View Omni transactions on the Bitcoin network",
        "Lookup Omni asset (smart property) information",
        "View asset trading on the distributed exchange (DEx)"
      ],
      link: "https://omniexplorer.info/",
      text: "Visit Site"
    },
    {
      additionalClass: "listing-3",
      image: "images/logo.svg",
      title: "Omni Core",
      list: [
        "Fully-validating desktop wallet",
        "A superset of Bitcoin Core",
        "Mac OS X, Windows, and Linux",
        "Native, cross-platform user interface",
        "Peer-to-peer distributed"
      ],
      link: "https://www.omnilayer.org/download.html",
      text: "Download"
    }
  ];