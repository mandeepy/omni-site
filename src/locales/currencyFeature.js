export const CURRENCY_FEATURE = [
    {
      image: "images/icons/lock-icon.png",
      title: "Easy to use, secure web wallets available"
    },
    {
      image: "images/icons/integrate.png",
      title: "Integrated with top Bitcoin and Alt-coin Exchanges"
    },
    {
      image: "images/icons/bitcoin.png",
      title: "Easy to integrate server daemon based on Bitcoin Core"
    },
    {
      image: "images/icons/screen-icon.png",
      title: "Fully-validating desktop wallet and client based on Bitcoin Qt"
    },
    {
      image: "images/icons/tether-dollar.png",
      title: "Tether Dollars backed by Bank Trust, redeemable for SWIFT at tether.to and bitfinex.com"
    },
    {
      image: "images/icons/dollar.png",
      title: "Over 1200M USD in asset market cap on the layer as of 2017"
    }
  ]