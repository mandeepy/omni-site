export const HEADER_LINKS=[
  {
    title:'About',
    id:'#about',
  },
  {
    title:'Products',
    id:'#products',
  },
  {
    title:'Projects',
    id:'#projects',
  },
  {
    title:'Testimonials',
    id:'#testimonials',
  },
  {
    title:'Team',
    id:'#team',
  },
]