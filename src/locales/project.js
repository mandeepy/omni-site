export const PROJECT = [
  {
    title: "Omni Protocol Specification",
    description: "The Specification for the Omni Layer protocol.",
    url: "https://github.com/OmniLayer/spec",
    alt: "omni-logo",
    image: "images/logo.svg",
  },
  {
    title: "Omni Core",
    description: "The Omni reference implementation. A C++ superset of Bitcoin Core.",
    url: "https://github.com/OmniLayer/omnicore",
    alt: "omni-logo",
    image: "images/logo.svg",
  },
  {
    title: "Omni Wallet",
    description: "Hosted wallet server in Python with AngularJS front-end.",
    url: "https://github.com/OmniLayer/omniwallet",
    alt: "omni-logo",
    image: "images/logo.svg",
  },
  {
    title: "Omni J",
    description: "A Java/JVM Omni client.",
    url: "https://github.com/OmniLayer/OmniJ",
    alt: "omni-logo",
    image: "images/logo.svg",
  },
  {
    title: "Omni JS",
    description: "Node.js-based JSON-RPC client for Omni Core.",
    url: "https://github.com/OmniLayer/OmniTradeJS",
    alt: "omni-logo",
    image: "images/logo.svg",
  },
  {
    title: "See More...",
    description: "Official Omni Foundation GitHub projects.",
    url: "https://github.com/OmniLayer/",
    alt: "omni-logo",
    image: "images/logo.svg",
  }
]