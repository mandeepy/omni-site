const path = require('path');
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  return new Promise((resolve, reject) => {
    const postTemplate = path.resolve('src/templates/post.js');
    resolve(
      graphql(`{
        allMarkdownRemark (sort: { fields: [frontmatter___date], order: DESC }) {
              edges {
                node {
                  html
                  id
                  frontmatter {
                    path
                    title
                  }
                }
              }
            }
      }`)
      .then(result => {
        if (result.errors) {
          return reject(result.errors);
        }
        const posts = result.data.allMarkdownRemark.edges;

        posts.forEach( ({node}) => {
          createPage({
            path: node.frontmatter.path,
            component: postTemplate
          })
        })
      })
    );
  });
};

/* Allows named imports */
exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    },
  });
};

// RSS node
exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}