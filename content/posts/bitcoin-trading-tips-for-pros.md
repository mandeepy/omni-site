---
published: true
date: 2020-01-27T20:48:53Z
featured: false
path: "/blogpost5"
cover: "../../static/uploads/1.png"
title: Bitcoin Trading Tips for Pros
author_name: Craig Sellers
author_image: "../../static/uploads/author.png"
sub_title: ""

---
# Bitcoin Trading Tips for Pros

The talented team at Rana Dev ([https://ranadev.io](https://ranadev.io "https://ranadev.io")) has been engaged to create a new, updated version of omniwallet.org. The Omni team asked them to focus on usability and streamlining the most used aspects of the wallet, and to make the wallet work on mobile web. Initial prototypes are looking amazing and the team is pleased to share some examples of the new interface below:

  
![](/uploads/layer-3.png)

# Omniwallet-Mobile

We have also begun working on mobile versions of OmniWallet, testing binaries are available in our repo. OmniWallet-mobile is a native wallet for Omni Layer assets, released on both Android and iOS. It currently supports Bitcoin and all Omni Layer tokens, providing Bitcoin & Omni Layer users with an ease-of-use and unrivaled blend of security.

![](/uploads/layer-5.png)

It runs as an independent process connecting to a full node of Omni Core (version 0.7.0), which provides the on-chain services for Omni transactions on the Bitcoin network. The current OBD implementation is tightly-bound to Omni Core. Also currently being released is an API that enables light clients to communicate with OBD network. It is an easy way for our community to develop Lightning applications.